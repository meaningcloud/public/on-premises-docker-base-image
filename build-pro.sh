#!/bin/bash

IMAGE="meaningcloud/onprem"
VERSION="7.4n"

docker build -f Dockerfile-pro \
             --no-cache -t $IMAGE:$VERSION .
docker tag                 $IMAGE:$VERSION $IMAGE:latest
docker push                $IMAGE:$VERSION
docker push                $IMAGE:latest