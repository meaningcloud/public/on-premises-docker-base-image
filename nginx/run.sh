#!/bin/bash

echo "Starting php7-4.fpm..."
/etc/init.d/php7.4-fpm start

echo "Starting nginx..."
nginx -g "daemon off;"
