#!/bin/bash

IMAGE="meaningcloud/onprem-test"
VERSION="focal-7.4"

docker build -f Dockerfile-test \
             --no-cache -t $IMAGE:$VERSION .
docker tag                 $IMAGE:$VERSION $IMAGE:latest
docker push                $IMAGE:$VERSION
docker push                $IMAGE:latest
