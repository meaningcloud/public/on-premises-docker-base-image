#!/bin/bash

IMAGE="meaningcloud/onprem-test"
VERSION="apache-focal-7.4"

docker build -f Dockerfile-test-apache \
             --no-cache -t $IMAGE:$VERSION .
docker tag                 $IMAGE:$VERSION $IMAGE:apache-latest
docker push                $IMAGE:$VERSION
docker push                $IMAGE:apache-latest
